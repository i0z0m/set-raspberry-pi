.PHONY: all install clone provision backup encrypt decrypt help

SSH_USER=$(shell whoami)
HOSTNAME=$(shell hostname)
IP_ADDRESS=$(shell hostname -I | awk '{print $$1}')
SSH_PORT=22
PRIVATE_KEY_FILE=~/.ssh/id_ed25519_raspberrypi
MINECRAFT_DIR=/var/www/minecraft
CONFIG_FILES=$(MINECRAFT_DIR)/server.properties

all: install clone provision backup

install:
	sudo apt-get update
	sudo apt-get install -y ansible
	echo "[servers]" > ansible/inventory
	echo "$(HOSTNAME) ansible_host=$(IP_ADDRESS) ansible_user=$(SSH_USER) ansible_port=$(SSH_PORT) ansible_ssh_private_key_file=$(PRIVATE_KEY_FILE)" >> ansible/inventory
	echo "Inventory file created at ansible/inventory"
	curl -fsSL https://pkgs.tailscale.com/stable/debian/bookworm.noarmor.gpg | sudo tee /usr/share/keyrings/tailscale-archive-keyring.gpg >/dev/null
	curl -fsSL https://pkgs.tailscale.com/stable/debian/bookworm.tailscale-keyring.list | sudo tee /etc/apt/sources.list.d/tailscale.list
	sudo apt-get update
	sudo apt-get install -y tailscale
	sudo tailscale up --accept-dns=false
	sudo mkdir -p --mode=0755 /usr/share/keyrings
	curl -fsSL https://pkg.cloudflare.com/cloudflare-main.gpg | sudo tee /usr/share/keyrings/cloudflare-main.gpg >/dev/null
	echo 'deb [signed-by=/usr/share/keyrings/cloudflare-main.gpg] https://pkg.cloudflare.com/cloudflared bookworm main' | sudo tee /etc/apt/sources.list.d/cloudflared.list
	sudo apt-get update
	sudo apt-get install -y cloudflared

clone:
	sudo mkdir -p $(MINECRAFT_DIR)
	sudo chown $(USER):$(USER) $(MINECRAFT_DIR)
	if [ ! -d "$(MINECRAFT_DIR)/.git" ]; then \
		git clone https://github.com/itzg/docker-minecraft-server.git $(MINECRAFT_DIR); \
	fi

provision:
	ansible-playbook -i ansible/inventory ansible/playbooks/common.yml --ask-become-pass
	ansible-playbook -i ansible/inventory ansible/playbooks/security.yml --ask-become-pass
	ansible-playbook -i ansible/inventory ansible/playbooks/monitoring.yml --ask-become-pass

encrypt:
	ansible-vault encrypt $(CONFIG_FILES)

decrypt:
	ansible-vault decrypt $(CONFIG_FILES)

help:
	@echo "Available targets:"
	@echo "  all       - Install, clone, provision, and backup"
	@echo "  install   - Update and install necessary packages"
	@echo "  clone     - Clone the Minecraft repository if it doesn't exist"
	@echo "  provision - Provision the server using Ansible"
	@echo "  encrypt   - Encrypt configuration files"
	@echo "  decrypt   - Decrypt configuration files"
